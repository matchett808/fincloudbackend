const Minio = require('minio');
const mqttClient = require('mqtt');

const mqtt = new mqttClient.connect('mqtt://127.0.0.1');
const minioClient = new Minio.Client({
    endPoint: '127.0.0.1',
    port: 9000,
    useSSL: false,
    accessKey: 'FinCloudIsAwesome',
    secretKey: 'SeriouslyAwesome'
});

/* subscribe to interesting topics */
mqtt.on('connect', () => {
    mqtt.subscribe('files/incoming');
});

/* Message Handling section */
mqtt.on('message', (topic, message) => {
    console.log(topic);
    switch(topic) {
        case 'files/incoming':
                syncRequest();
            break;
    }

});

let syncRequest = () => {
    let objectStream = minioClient.listObjects('inbound-data');
    objectStream.on('data', (storedObject) => {
        /* here it's time to test if this has been processed, then process if not */
    });
}